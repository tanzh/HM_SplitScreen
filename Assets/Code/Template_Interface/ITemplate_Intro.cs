﻿using UnityEngine;
using System.Collections;

public interface ITemplate_Intro{
    /// <summary>
    /// 传入数据
    /// </summary>
    /// <param name="value"></param>
    void GetData(object value);
    /// <summary>
    /// 是否显示
    /// </summary>
    /// <param name="isShow"></param>
    void Show(bool isShow = true);
}
