﻿using UnityEngine;
using System.Collections;

public interface ITemplate_Info  {
    /// <summary>
    /// 传入数据
    /// </summary>
    /// <param name="value"></param>
    void getData(object value);
    /// <summary>
    /// 是否显示
    /// </summary>
    /// <param name="isShow"></param>
    void Show(bool isShow = true);
    /// <summary>
    /// 移动控制
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="speed"></param>
    void Run(float speed,bool isLeft = true);
    /// <summary>
    /// 获取当前物体的属性
    /// </summary>
    /// <returns></returns>
    RectTransform getRect();
}
