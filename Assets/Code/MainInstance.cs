﻿using UnityEngine;
using System.Collections;
using System;

public class MainInstance : MonoBehaviour {
    
    public Vector2 屏幕分辨率;
    public bool 是否使用账号登陆 = false;
    public bool release = false;

    private readonly string _userID = "code";
    private readonly string _userKey = "4er6tyhf89olk8qkmc098u7";

    // Use this for initialization
    void Start () {
        //初始化配置文件
        InitConfig.GetInstance().Init(_userID, _userKey, InitConfig.GetInstance().SERVER_GET_DATA);

        if (release)
            屏幕分辨率 = new Vector2(Screen.currentResolution.width, Screen.currentResolution.height);

        #region 操作界面
        new LoadingScreenController().setResolution(屏幕分辨率);
        new SplitScreenController().setResolution(屏幕分辨率);
        #endregion
        init();
	}

    private void init()
    {
        Screen.SetResolution((int)屏幕分辨率.x, (int)屏幕分辨率.y, true);
        //程序入口方法
        GameStatusModel.GetInstance().ScreenStatus = GameStatusModel.SCREEN_LOADING;
    }
}