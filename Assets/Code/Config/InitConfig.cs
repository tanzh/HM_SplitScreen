﻿using UnityEngine;
using System.Collections;

public class InitConfig
{
    public string SERVER_GET_DATA { get { return "http://115.29.249.151/Gorgeous/index.php/Home/User/index"; } }
    public string SERVER_GET_QR { get { return ""; } }

    public string USER_ID;
    public string USER_KEY;
    public string End_Point;
    #region 单例及初始化
    /// <summary>
    /// 单例及初始化
    /// </summary>
    private static InitConfig _instance = null;
    public static InitConfig GetInstance()
    {
            if(_instance==null)
            {
                _instance = new InitConfig();
            }
            return _instance;    
    }

    private InitConfig()
    {
       
    }
    #endregion

    public void Init(string user_id,string user_key,string end_point)
    {
        USER_ID = user_id;
        USER_KEY = user_key;
        End_Point = end_point;
    }
}
