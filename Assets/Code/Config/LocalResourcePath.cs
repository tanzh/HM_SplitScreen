﻿using UnityEngine;
using System.Collections;

public class LocalResourcePath
{
    
    private string persionImage = "Doctor";
    private string persionConfig = "PersionConfig";
    private string configFileName = "DoctorConfig";
    private string background = "BackGround";
    
    private readonly string dataPath = null;
    private readonly string persistentDataPath = null;

    #region 单例及初始化
    /// <summary>
    /// 单例及初始化
    /// </summary>
    private static LocalResourcePath _instance = null;
    public static LocalResourcePath GetInstance()
    {
        if (_instance == null)
            _instance = new LocalResourcePath();
        return _instance;
    }

    private LocalResourcePath()
    {
        dataPath = Application.dataPath;
        dataPath = dataPath.Remove(dataPath.LastIndexOf("/")) + "/resource";
        persistentDataPath = Application.persistentDataPath;
    }
    #endregion

    /// <summary>
    /// 获取对象主图路径
    /// </summary>
    /// <param name="partName">遵循".../a/b/..."的格式</param>
    /// <returns></returns>
    public string PersonImage(string partName)
    {
        return persistentDataPath + partName + persionImage;
    }

    /// <summary>
    /// 获取对象配置路径
    /// </summary>
    /// <param name="partName">遵循".../a/b/..."的格式</param>
    /// <returns></returns>
    public string PersonConfig(string partName)
    {
        return persistentDataPath + partName + persionConfig;
    }

    public string BackGround
    {
        get
        {
            return dataPath + "/" + background;
        }
    }

    /// <summary>
    /// 获取本地配置文件
    /// </summary>
    /// <param name="isFullName"></param>
    /// <returns></returns>
    public string ConfigPath(bool isFullName = false)
    {
        if (isFullName)
            //return persistentDataPath + "/Configs/" + configFileName;
            return persistentDataPath + "/" + configFileName;
        else
            //return persistentDataPath + "/Configs";
            return persistentDataPath;
    }

    public string ResourceRoot
    {
        get
        {
            return persistentDataPath;
        }
    }
}
