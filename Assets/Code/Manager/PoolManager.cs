﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour {

    public ProfileView[] list;

    void Start()
    {
        StartCoroutine_Auto(repeat());
    }

    private IEnumerator repeat()
    {
        while (true)
        {
            list = GetComponentsInChildren<ProfileView>();
            for (int i = 0; i < list.Length; i++)
            {
                foreach (var item in list[i].GetComponentsInChildren<BoxCollider>())
                {
                    item.size = new Vector3(item.size.x, item.size.y, i + 1);
                } 
            }
            yield return new WaitForSeconds(0.02f);
        }
    }
}
