﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class LoadingMaskAnimPanel : MonoBehaviour {

    [SerializeField]
    private string label = "";
    [SerializeField]
    private float interval = 0.5f;
    [SerializeField]
    private Text tis;
    private string str = "";
    private int currentLenght = -1;
    private int count = 0;
    private bool isRun = false;
    void Start()
    {
        tis = this.GetComponent<Text>();
    }

    public void StartLoop(bool _run = false)
    {
        isRun = _run;
        if (isRun)
        {
            transform.parent.gameObject.SetActive(true);
            StartCoroutine_Auto(LoopLetter());
        }
        else
        {
            StopCoroutine(LoopLetter());
            transform.parent.gameObject.SetActive(false);
        }     
    }

    IEnumerator LoopLetter()
    {
        while (isRun)
        {
            if (tis.text.Length > 0)
            {
                if(currentLenght == str.Length)
                {
                    if(count >= currentLenght - 1)
                    {
                        count = 0;
                        label = "";
                    }
                    else
                    {
                        count++;
                    }
                }
                else
                {
                    currentLenght = tis.text.Length;
                    str = tis.text.ToUpperInvariant();
                    label = "";
                    count = 0;
                }
               
                label+= str[count].ToString();
                tis.text = label;
            }
            yield return new WaitForSeconds(interval);
        }
    }

    public string setLabel
    {
        get { return tis.text; }
        set { tis.text = value; }
    }
}
