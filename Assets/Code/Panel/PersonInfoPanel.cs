﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using TouchScript.Gestures;

public class MainInfoPanel : MonoBehaviour
{
    [SerializeField]
    private RawImage _hand = null;
    [SerializeField]
    private Text _name = null;
    [SerializeField]
    private Text _jobDes = null;
    [SerializeField]
    private TapGesture _more = null;

    /// <summary>
    /// 头像
    /// </summary>
    protected Texture Hand
    {
        set {         
            _hand.texture = value;
            if (value == null) return;
            float scale = _hand.GetComponent<RectTransform>().sizeDelta.y / value.height;
            _hand.GetComponent<RectTransform>().sizeDelta = new Vector2(value.width * scale, _hand.GetComponent<RectTransform>().sizeDelta.y);
        }
    }

    /// <summary>
    /// 名字
    /// </summary>
    protected string Name
    {
        set { _name.text = value; }
    }

    /// <summary>
    /// 职位
    /// </summary>
    protected string JobDes
    {
        set { _jobDes.text = value; }
    }

    /// <summary>
    /// More按钮
    /// </summary>
    protected TapGesture Btn_More
    {
        get { return _more; }
    }

    void Start()
    {
        startFunc();
    }

    void Update()
    {
        updateFunc();
    }

    protected virtual void startFunc()
    {

    }

    protected virtual void updateFunc()
    {

    }
}
