﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
/// <summary>
/// 下载界面
/// </summary>
public class LoadingScreenPanel : MonoBehaviour {

    /// <summary>
    /// 背景
    /// </summary>
    [SerializeField]
    private RawImage m_background = null;

    /// <summary>
    /// 滑动条
    /// </summary>
    [SerializeField]
    private Slider m_slider = null;

    /// <summary>
    /// 加载百分比
    /// </summary>
    [SerializeField]
    private Text m_percentage = null;

    protected Texture BackGround
    {
        private get { return m_background.texture; }
        set { m_background.texture = value; }
    }

    protected float SliderValue
    {
        get { return m_slider.value; }
        set { m_slider.value = value; }
    }

    protected string Percentage
    {
        get { return m_percentage.text; }
        set { m_percentage.text = value; }
    }

    void Start()
    {
        startFunc();
    }

    void Update()
    {
        updateFunc();
    }

    protected virtual void startFunc()
    {

    }

    protected virtual void updateFunc()
    {

    }
}
