﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using TouchScript.Gestures;

public class ProfilePanel : MonoBehaviour {

    [SerializeField]
    private RawImage _hand = null;
    [SerializeField]
    private Text _name = null;
    [SerializeField]
    private Text _leve = null;
    [SerializeField]
    private Text _jobMain = null;
    [SerializeField]
    private Text _jobDes = null;
    [SerializeField]
    private Text _intro = null;
    [SerializeField]
    private Text _specialty = null;
    [SerializeField]
    private TapGesture _close = null;
    [SerializeField]
    private TapGesture _tapGes = null;
    [SerializeField]
    private PanGesture _panGes = null;
    [SerializeField]
    private PanGesture _panGesIntro = null;

    /// <summary>
    /// 头像
    /// </summary>
    public Texture Hand
    {
        set
        {
            _hand.texture = value;
            if (value == null) return;
            float scale = _hand.GetComponent<RectTransform>().sizeDelta.y / value.height;
            _hand.GetComponent<RectTransform>().sizeDelta = new Vector2(value.width * scale, _hand.GetComponent<RectTransform>().sizeDelta.y);
        }
    }
    /// <summary>
    /// 名字
    /// </summary>
    public string Name
    {
        set { _name.text = value; }
    }
    /// <summary>
    /// 等级
    /// </summary>
    public string Level
    {
        set { _leve.text = value; }
    }


    /// <summary>
    /// 明星医生
    /// </summary>
    public string JobMain
    {
        set { _jobMain.text = value; }
    }

    /// <summary>
    /// 职位
    /// </summary>
    public string JobDes
    {
        set { _jobDes.text = value; }
    }
    /// <summary>
    /// 职称
    /// </summary>
    public string Intro
    {
        set
        {
            _intro.text = value;
            float height = _intro.preferredHeight;
            float parH = _intro.transform.parent.GetComponent<RectTransform>().sizeDelta.y;
            if (height <= parH)
            {
                _intro.transform.parent.GetComponent<BoxCollider>().enabled = false;
                return;
            }
            RectTransform rt = _intro.GetComponent<RectTransform>();
            _intro.GetComponent<RectTransform>().sizeDelta = new Vector2(rt.sizeDelta.x, height);
        }
    }
    /// <summary>
    /// 特长
    /// </summary>
    public string Specialty
    {
        set { _specialty.text = value; }
    }

    #region 触屏事件按钮
    //触屏事件按钮
    public TapGesture Tap_Close
    {
        get { return _close; }
    }

    public TapGesture TapGes
    {
        get { return _tapGes; }
    }

    public PanGesture PanGes
    {
        get { return _panGes; }
    }

    public PanGesture PanGesIntro
    {
        get { return _panGesIntro; }
    }
    #endregion

    void Start()
    {
        startFunc();
    }

    void Update()
    {
        updateFunc();
    }

    protected virtual void startFunc()
    {

    }

    protected virtual void updateFunc()
    {

    }
}
