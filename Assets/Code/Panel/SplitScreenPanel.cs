﻿using UnityEngine;
using System;
using UnityEngine.UI;

/// <summary>
/// 产品展示界面
/// </summary>
public class SplitScreenPanel : MonoBehaviour {
    /// <summary>
    /// 背景
    /// </summary>
    [SerializeField]
    private RawImage m_background = null;

    /// <summary>
    /// 一级菜单父节点
    /// </summary>
    [SerializeField]
    private RectTransform m_MainPool = null;

    /// <summary>
    /// 二级菜单父节点
    /// </summary>
    [SerializeField]
    private RectTransform m_OtherPool = null;

    /// <summary>
    /// 遮罩层Label
    /// </summary>
    [SerializeField]
    private LoadingMaskAnimPanel m_MaskLabel = null;

    protected Texture BackGround
    {
        private get { return m_background.texture; }
        set { m_background.texture = value;}
    }

    protected RectTransform MainPool
    {
        get { return m_MainPool; }
        set { m_MainPool = value; }
    }

    protected RectTransform OtherPool
    {
        get { return m_OtherPool; }
        set { m_OtherPool = value; }
    }

    protected string MaskLabel
    {
        get { return m_MaskLabel.setLabel; }
        set { m_MaskLabel.setLabel = value; }
    }

    protected void PlayLoadingAnim(bool isComplete = false)
    {
        m_MaskLabel.StartLoop(isComplete);
    }

    void Start()
    {
        startFunc();
    }

    void Update()
    {
        updateFunc();
    }

    protected virtual void startFunc()
    {

    }

    protected virtual void updateFunc()
    {

    }
}
