﻿using System;
using System.Collections.Generic;

public interface IData
{
    /// <summary>
    /// 传入JSON文件
    /// </summary>
    /// <param name="json">JSON文件</param>
    void Translate2String(string json,Action callBack); 
    /// <summary>
    /// 获取所有数据
    /// </summary>
    /// <typeparam name="T">数据类型</typeparam>
    /// <returns></returns>
    Dictionary<string,object> GetData();
    /// <summary>
    /// 加载进度
    /// </summary>
    /// <returns></returns>
    float LoadingProgress { get; set; }
    /// <summary>
    /// 侦听加载进行过程
    /// </summary>
    DelegateEvent.DataChangeEvent AddPrecentEvent { get;set; }
}