﻿using UnityEngine;
using System.Collections;
using System.IO;

public class LoadingScreenView:LoadingScreenPanel
{
    protected override void startFunc()
    {
        SliderValue = 0;
        getBG();
        base.startFunc();
    }

    public float setSliderValue
    {
        set
        {
            SliderValue = value * 100;
            Percentage = SliderValue.ToString("0.0") + "%";
        }
    }

    /// <summary>
    /// 切换背景或获取背景
    /// </summary>
    private void getBG()
    {
        string path = LocalResourcePath.GetInstance().BackGround;
        if (!Directory.Exists(path)) Directory.CreateDirectory(path);

        string[] bgImages = Directory.GetFiles(path, "*.jpg");
        if (bgImages.Length > 0)
        {
            Loader.GetInstance().SendMessage_GET("file:///" + bgImages[0], setBG);
        }
        else
        {
            Texture tex = Resources.Load<Texture>("Texture/defaultBG");
            BackGround = tex;
        }
    }

    private void setBG(WWW www)
    {
        if (www.error == null)
        {
            BackGround = www.texture;
        }
        else
        {
            Debug.Log(www.error);
        }
    }
}
