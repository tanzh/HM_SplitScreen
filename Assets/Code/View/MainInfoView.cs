﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class MainInfoView : MainInfoPanel, ITemplate_Info
{
    private UnityEngine.Object personIntro = null;
    private string id = "";
    private object serverData;
    private RectTransform rect;
    protected override void startFunc()
    {
        rect = this.GetComponent<RectTransform>();
        listener();
        base.startFunc();
    }

    private void listener()
    {
        Btn_More.Tapped += OnMoreBtnClick;
    }

    //按钮事件
    private void OnMoreBtnClick(object sender, EventArgs e)
    {
        personIntro = Resources.Load("Prefabs/PersionIntro", typeof(GameObject));
        RectTransform root = GameObject.Find("SplitScreen").GetComponent<SplitScreenView>().OtherRoot;
        GameObject obj = Instantiate(personIntro) as GameObject;
        obj.name = id;
        obj.transform.SetParent(root);
        obj.GetComponent<RectTransform>().localScale = Vector3.one;
        obj.GetComponent<RectTransform>().position = rect.position;

        ITemplate_Intro item = obj.GetComponent<ProfileView>();
        item.GetData(serverData);
        item.Show();
    }

    public void getData(object value)
    {
        serverData = value;
        Person data = (Person)value;
        id = data.id;
        Name = data.name;
        JobDes = data.jodDes;

        string path = "file:///" + LocalResourcePath.GetInstance().PersonImage(string.Format("/{0}/", id)) + "/index.png";
        Loader.GetInstance().SetImages(path, setHandImage);
    }

    private void setHandImage(WWW www)
    {
        if (www.error == null)
        {
            Hand = www.texture;
        }
        else
        {
            Hand = Resources.Load<Texture>("Texture/defaultHand");
        }
    }

    public void Show(bool isShow = true)
    {
        if (isShow)
        {
                
        }
        else
        {        
            Destroy(this, 3);
        }
    }

    public void Run(float speed, bool isLeft = true)
    {
        if (isLeft)
        {
            rect.anchoredPosition3D += new Vector3(-speed, 0, 0);
        }
        else
        {
            rect.anchoredPosition3D += new Vector3(speed, 0, 0);
        }
    }

    public RectTransform getRect()
    {
        if (rect != null) return rect;
        return null;
    }
}
