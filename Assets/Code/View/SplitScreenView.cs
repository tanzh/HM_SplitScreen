﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SplitScreenView : SplitScreenPanel {

    public int count = 0;
    public float interval = 100;
    public bool isRun = false;
    [Range(100,1000)]
    public uint runSpeed = 1;
    
    protected override void startFunc()
    {
        getBG();
        base.startFunc();
    }

    public void PlayAnim(bool isPlay = false)
    {
        PlayLoadingAnim(isPlay);
    }

    public RectTransform MainRoot
    {
        get { return MainPool; }
    }

    public RectTransform OtherRoot
    {
        get { return OtherPool; }
    }

    /// <summary>
    /// 切换背景或获取背景
    /// </summary>
    private void getBG()
    {
        string path = LocalResourcePath.GetInstance().BackGround;
        if (!Directory.Exists(path)) Directory.CreateDirectory(path);

        string[] bgImages = Directory.GetFiles(path, "*.jpg");
        if (bgImages.Length > 0)
        {
            Loader.GetInstance().SendMessage_GET("file:///" + bgImages[0], setBG);
        }
        else
        {
            Texture tex = Resources.Load<Texture>("Texture/defaultBG");
            BackGround = tex;
        }
    }

    private void setBG(WWW www)
    {
        if (www.error == null)
        {
            BackGround = www.texture;
        }
        else
        {
            Debug.Log(www.error);
        }
    }
}
