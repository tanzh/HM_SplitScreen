﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using TouchScript.Gestures;

public class ProfileView : ProfilePanel, ITemplate_Intro
{
    private string id;
    private Vector3 _offset;

    private CanvasGroup canvasG;
    private bool isRunShowFunc = false;
    private bool isDestroy = false;
    private float targetAlpha;

    private bool isRun = false;
    private bool isOffset = false;   
    private Vector3 targetPoint;
    private RectTransform rectChild;

    protected override void startFunc()
    {
        listener();
        canvasG = this.GetComponent<CanvasGroup>();
        base.startFunc();
    }

    protected override void updateFunc()
    {
        if (isOffset)
        {
            rectChild.anchoredPosition3D = Vector3.Lerp(rectChild.anchoredPosition3D, targetPoint, 0.3f);
            if (Vector3.Distance(rectChild.anchoredPosition3D, targetPoint) < 0.1f)
            {
                rectChild.anchoredPosition3D = targetPoint;
                isOffset = false;
                isRun = false;
            }
        }

        if (isRunShowFunc)
        {
            if (isDestroy)
            {
                //transform.Rotate(0, 0, 5);
                transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, 2f * Time.deltaTime);
                canvasG.alpha = Mathf.Lerp(canvasG.alpha, targetAlpha, 0.05f);
            }
            else
            {
                canvasG.alpha = Mathf.Lerp(canvasG.alpha, targetAlpha, 0.1f);
            }
            
            if (Mathf.Abs(canvasG.alpha - targetAlpha) < 0.1f)
            {
                canvasG.alpha = targetAlpha;
                isRunShowFunc = false;
            }
        }
    }

    private void listener()
    {
        Tap_Close.Tapped += OnCloseBtnClick;
        TapGes.Tapped += OnPanelTapToTop;
        PanGes.StateChanged += OnPanelPanTomove;
        PanGesIntro.StateChanged += OnPanelPanToUp;
    }

    //touchscript竖向平移代码
    private void OnPanelPanToUp(object sender, GestureStateChangeEventArgs e)
    {
        if(rectChild==null)
            rectChild = (sender as PanGesture).transform.GetChild(0) as RectTransform;
        //当文字高度没有超出父对象高度时，不执行下一步
        if (rectChild.sizeDelta.y <= ((sender as PanGesture).transform as RectTransform).sizeDelta.y) return;
        switch (e.State)
        {
            case Gesture.GestureState.Began:
                Vector3 mousePos = Camera.main.ScreenToWorldPoint((sender as PanGesture).ScreenPosition);
                _offset = rectChild.position - mousePos;
                break;
            case Gesture.GestureState.Changed:
                Vector3 touchOffset = (sender as PanGesture).ScreenPosition - (sender as PanGesture).PreviousScreenPosition;
                if (Mathf.Abs(touchOffset.x) < Mathf.Abs(touchOffset.y))
                {
                    //边界检测
                    float max = rectChild.sizeDelta.y - ((sender as PanGesture).transform as RectTransform).sizeDelta.y;
                    isOffset = false;
                    if (rectChild.anchoredPosition3D.y < 0 || rectChild.anchoredPosition3D.y > max)
                    {                 
                        if (rectChild.anchoredPosition3D.y < 0)
                            targetPoint = new Vector3(0, 0, 0);
                        if (rectChild.anchoredPosition3D.y > max)
                            targetPoint = new Vector3(0, max, 0);
                        isRun = true;                          
                    }  
                                  
                    Vector3 targetPos = Camera.main.ScreenToWorldPoint((sender as PanGesture).ScreenPosition) + _offset;
                    rectChild.position = Vector3.Lerp(rectChild.position,new Vector3(rectChild.position.x, targetPos.y, rectChild.position.z), .5f);
                }
                break;
            case Gesture.GestureState.Recognized:
                if(isRun) isOffset = true;
                break;
        }
    }

    //touchscript横向平移代码
    private void OnPanelPanTomove(object sender, TouchScript.Gestures.GestureStateChangeEventArgs e)
    {
        //Debug.Log((sender as PanGesture).name);
        switch (e.State)
        {  
            case Gesture.GestureState.Began:
                Debug.Log((sender as PanGesture).name);
                transform.GetComponent<RectTransform>().SetAsLastSibling();
                Vector3 mousePos = Camera.main.ScreenToWorldPoint((sender as PanGesture).ScreenPosition);
                _offset = transform.position - mousePos;
                break;
            case Gesture.GestureState.Changed:
                Vector3 touchOffset = (sender as PanGesture).ScreenPosition - (sender as PanGesture).PreviousScreenPosition;             
                //RectTransform rt = GetComponent<RectTransform>();
                //float distance = 540 - rt.sizeDelta.y / 2;
                //float targetY = rt.anchoredPosition3D.y > distance ? distance : rt.anchoredPosition3D.y;
                //targetY = rt.anchoredPosition3D.y < -distance ? -distance : targetY;
                //rt.anchoredPosition3D = new Vector3(rt.anchoredPosition3D.x, targetY, rt.anchoredPosition3D.z);
                if (Mathf.Abs(touchOffset.x) > Mathf.Abs(touchOffset.y))
                {
                    Vector3 targetPos = Camera.main.ScreenToWorldPoint((sender as PanGesture).ScreenPosition) + _offset;
                    this.transform.position = Vector3.Lerp(this.transform.position, new Vector3(targetPos.x, this.transform.position.y, this.transform.position.z), .5f);
                }
                break;
        }
    }

    private void OnPanelTapToTop(object sender, EventArgs e)
    {
        //点击后移动到最上层
        transform.GetComponent<RectTransform>().SetAsLastSibling();
    }

    //按钮事件
    private void OnCloseBtnClick(object sender, EventArgs e)
    {
        Show(false);
    }

    //------------------
    /// <summary>
    /// 获取数据
    /// </summary>
    /// <param name="value"></param>
    public void GetData(object value)
    {
        Person data = (Person)value;
        id = data.id;
        Name = data.name;
        Level = data.level;    
        //Intro 
        Specialty = data.speciality;
        JobDes = data.jodDes;
        string jobDescribe = data.intro;
        JobMain = jobDescribe.Remove(jobDescribe.IndexOf("\r\n"));
        Intro = jobDescribe.Substring(jobDescribe.IndexOf("\r\n")+4);



        string path = "file:///" + LocalResourcePath.GetInstance().PersonImage(string.Format("/{0}/", id)) + "/index.png";
        Loader.GetInstance().SetImages(path, setHandImage);
    }

    private void setHandImage(WWW www)
    {
        if (www.error == null)
        {
            Hand = www.texture;
        }
        else
        {
            Hand = Resources.Load<Texture>("Texture/defaultHand"); 
        }
    }
    /// <summary>
    /// 显示特效
    /// </summary>
    /// <param name="isShow"></param>
    public void Show(bool isShow = true)
    {
        if (isShow)
        {
            targetAlpha = 1;           
        }
        else
        {
            targetAlpha = 0;
            isDestroy = true;
            Destroy(this.gameObject,5);
            Resources.UnloadUnusedAssets();
        }
        isRunShowFunc = true;
    }

    
}
