﻿using UnityEngine;
using System.Collections.Generic;

public class LoadingModel
{
    private static LoadingModel _instance = null;
    public static LoadingModel GetInstance()
    {
        if (_instance == null)
            _instance = new LoadingModel();
        return _instance;
    }

    //public event DelegateEvent.DataChangeEvent OnLoadingServerData;
    private Dictionary<string, object> _serverData = new Dictionary<string, object>();
    public Dictionary<string,object> ServerData
    {
        get { return _serverData; }
        set
        {
            _serverData = value;
            //if (OnLoadingServerData != null) OnLoadingServerData(_serverData);
        }
    }
}

