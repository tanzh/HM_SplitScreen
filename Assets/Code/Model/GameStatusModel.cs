﻿using UnityEngine;
using System.Collections;

public class GameStatusModel
{
    public const string SCREEN_LOADING = "下载界面";
    public const string SCREEN_PLAY = "游戏界面";

    private static GameStatusModel _instance = null;
    public static GameStatusModel GetInstance()
    {
        if (_instance == null)
            _instance = new GameStatusModel();
        return _instance;
    }

    public event DelegateEvent.ScreenStatusChangeEvent OnStatusChangeEvent;
    private string _screenStatus = null;
    public string ScreenStatus
    {
        get { return _screenStatus; }
        set
        {
            if (_screenStatus == value) return;
            _screenStatus = value;
            if (OnStatusChangeEvent != null) OnStatusChangeEvent(_screenStatus);
        }
    }
}
