﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Loader : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    #region 单例
    private static Loader _instance = null;
    /// <summary>
    /// 单例
    /// </summary>
    public static Loader GetInstance()
    {
        if (!_instance)
        {
            _instance = (Loader)FindObjectOfType(typeof(Loader));
            if (!_instance)
                _instance = new GameObject("Loader").AddComponent(typeof(Loader)) as Loader;
        }
        return _instance;
    }
    #endregion

    #region 发送信息
    /// <summary>
    /// POST 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="wf"></param>
    /// <param name="callBack"></param>
    public void SendMessage_POST(string path,WWWForm wf,Action<WWW> callBack)
    {
        StartCoroutine_Auto(sendmessage_post(path, wf, callBack));
    }

    private IEnumerator sendmessage_post(string path,WWWForm wf,Action<WWW> callBack)
    {
        using(WWW www = new WWW(path, wf))
        {
            yield return www;
            callBack(www);
        }
        Resources.UnloadUnusedAssets();
    }

    /// <summary>
    /// GET
    /// </summary>
    /// <param name="path"></param>
    /// <param name="callBack"></param>
    public void SendMessage_GET(string path,Action<WWW> callBack)
    {
        StartCoroutine_Auto(sendmessage_post(path, callBack));
    }

    private IEnumerator sendmessage_post(string path,Action<WWW> callBack)
    {
        using (WWW www = new WWW(path))
        {
            yield return www;
            callBack(www);
        }
        Resources.UnloadUnusedAssets();
    }
    #endregion

    #region 其他操作
    /// <summary>
    /// 设置图片
    /// </summary>
    /// <param name="filePath"></param>
    /// <param name="textures"></param>
    /// <param name="callBack"></param>
    public void SetImages(string filePath,RawImage[] textures, Action<WWW, RawImage[]> callBack)
    {
        StartCoroutine_Auto(setImages(filePath, textures, callBack));
    }

    private IEnumerator setImages(string filePath,RawImage[] textures,Action<WWW,RawImage[]> callBack)
    {
        using (WWW www = new WWW(filePath))
        {
            yield return www;
            foreach (var item in textures)
            {
                if(item!=null)
                {
                    item.texture = www.texture;
                    RectTransform rt = item.GetComponent<RectTransform>();
                    rt.sizeDelta = new Vector2(www.texture.height * rt.sizeDelta.x / www.texture.width, rt.sizeDelta.y);
                }
            }
            callBack(www, textures);
        }
        Resources.UnloadUnusedAssets();
    }

    /// <summary>
    /// 设置图片
    /// </summary>
    /// <param name="filePath"></param>
    /// <param name="callBack"></param>
    public void SetImages(string filePath,Action<WWW> callBack)
    {
        SendMessage_GET(filePath, callBack);
    }

    /// <summary>
    /// 协同方法(适用于：1.延迟操作 2.重复执行)
    /// </summary>
    /// <param name="SynergyFunc"></param>
    public void SetSynergyFunc(IEnumerator SynergyFunc)
    {
        StartCoroutine_Auto(SynergyFunc);
    }
    #endregion
}
