﻿using UnityEngine;
using System.Collections;

public class DelegateEvent
{
    /// <summary>
    /// 数据变化
    /// </summary>
    /// <param name="value"></param>
    public delegate void DataChangeEvent(object value);
    /// <summary>
    /// 游戏状态
    /// </summary>
    /// <param name="status"></param>
    public delegate void ScreenStatusChangeEvent(string status);
}
